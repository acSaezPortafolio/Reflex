package com.wxsoftware.reflex.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wxsoftware.reflex.R;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedDeque;


/**
 * Created by alberto on 22/02/18.
 *
 * Clase que extiende de view con la que podemos tener mucha flexibilidad a la hora de crear
 * views, ya que podremos crear cualquier tipo de view que queramos.
 */

public class ReflexView extends View {

    //Instancias estáticas que cambian
    private static final String HIGH_SCORE="HIGH_SCORE";
    private SharedPreferences preferences;

    //Variables que manejan el juego
    private int spotTouched;
    private int score;
    private int level;
    private int viewWidth;
    private int viewHeigh;
    private long animationTime;
    private boolean gameOver;
    private boolean gamePaused;
    private boolean dialogDisplayed;
    private int highScore;

    //Collección de círculos/spots y Animators
    private final Queue<ImageView> spots= new ConcurrentLinkedDeque<>();
    private final Queue<Animator> animators=new ConcurrentLinkedDeque<>();

    private TextView highScoreTextView, currentScoreTextView, levelTextView;
    private LinearLayout livesLinearLayout;
    private RelativeLayout mainRelativeLayout;
    private Resources resources;
    private LayoutInflater inflater;


    public static final int INITIAL_ANIMATION_DURATION=6000;
    public static final Random random=new Random();
    public static final int SPOT_DIAMETER=200;
    public static final float SCALE_X=0.25f;
    public static final float SCALE_Y=0.25f;
    public static final int INITIAL_SPOTS=5;
    public static final int SPOT_DELAY=500;
    public static final int LIVES=3;
    public static final int MAX_LIVES=7;
    public static final int NEW_LEVEL=10;
    private Handler spotHandler;

    public static final int HIT_SOUND_ID=1;
    public static final int MISS_SOUND_ID=2;
    public static final int DISAPPEAR_SOUND_ID=3;
    public static final int SOUND_PRIORITY=1;
    public static final int SOUND_QUALITY=100;
    public static final int MAX_STREAMS=4;

    private SoundPool.Builder soundPool;
    private int volume;
    private Map<Integer, Integer> soundMap;


    /**
     * Constructor
     * @param context
     * @param sharedPreferences
     * @param parentLayout
     */
    public ReflexView(Context context, SharedPreferences sharedPreferences,
                      RelativeLayout parentLayout) {
        super(context);

        preferences=sharedPreferences;
        highScore=preferences.getInt(HIGH_SCORE, 0);

        //salvar los recursos para cargar los valores externos
        resources=context.getResources();

        //salvar el LayoutInflater
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //configurar los componentes de la UI
        mainRelativeLayout=parentLayout;
        livesLinearLayout=mainRelativeLayout.findViewById(R.id.lifeLinearLayout);
        highScoreTextView=mainRelativeLayout.findViewById(R.id.highScoreTextView);
        currentScoreTextView=mainRelativeLayout.findViewById(R.id.scoreTextView);
        levelTextView=mainRelativeLayout.findViewById(R.id.levelTextView);

        spotHandler=new Handler();

    }




    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        viewWidth =w;
        viewHeigh=h;
    }





    public void pause(){
        gamePaused=true;
        soundPool.build().release();
        soundPool=null;
        cancelAnimations();
    }

    /**
     * Cuando el juego comienza
     * @param context
     */
    public void resume(Context context){
        gamePaused=false;
        initializeSoundEffects(context);

        if(!dialogDisplayed){
            resetGame();
        }
    }


    /**
     * Resetea el juego a los valores de inicio
     */
    public void resetGame(){
        spots.clear();//vacía la lista de spots
        animators.clear();//vacía la lista de animators
        livesLinearLayout.removeAllViews();//borra los antiguos spots de la pantalla

        animationTime=INITIAL_ANIMATION_DURATION;//inicia la duración de la animación

        spotTouched=0;//resetea el número de spots tocados
        score=0;//resetea la puntuación
        level=1;//resetea el nivel
        gameOver=false;//el juego no ha acabado
        displayScores();//muestra la puntuación y el nivel

        //añadimos las vidas
        for(int i=0; i<LIVES; i++){
            //añadimos el indicador de las vidas
            livesLinearLayout.addView(
                    (ImageView) inflater.inflate(R.layout.life, null)
            );
        }


        for (int i=1; i<=INITIAL_SPOTS; i++){
            spotHandler.postDelayed(addSpotRunnable, i*SPOT_DELAY);
        }
    }




    /**
     * Inicializa el pool de sonido para que puedan oirse
     * @param context
     */
    private void initializeSoundEffects(Context context){

        AudioAttributes aa=new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .setUsage(AudioAttributes.USAGE_GAME)
                .build();

        soundPool=new SoundPool.Builder();
        soundPool.setMaxStreams(MAX_STREAMS)
                .setAudioAttributes(aa)
                .build();


        //establecemos el volumen de los sonidos
        AudioManager manager= (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        volume=manager.getStreamVolume(AudioManager.STREAM_MUSIC);

        //creamos un mapa de sonidos
        soundMap=new HashMap<Integer, Integer>();
        soundMap.put(HIT_SOUND_ID, soundPool.build().load(context, R.raw.hit, SOUND_PRIORITY));
        soundMap.put(MISS_SOUND_ID, soundPool.build().load(context, R.raw.miss, SOUND_PRIORITY));
        soundMap.put(DISAPPEAR_SOUND_ID, soundPool.build().load(context, R.raw.disappear, SOUND_PRIORITY));
    }




    /**
     * Método que muestra las puntuaciones en pantalla
     */
    private void displayScores(){
        highScoreTextView.setText(resources.getString(R.string.highScoreTextView_text)+" "+highScore);
        currentScoreTextView.setText(resources.getString(R.string.scoreTextView_text)+" "+score);
        levelTextView.setText(resources.getString(R.string.levelTextView_text)+" "+level);
    }





    /**
     * Evento que se produce al tocar un spot
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(soundPool!= null){
            soundPool.build().play(HIT_SOUND_ID, volume, volume, SOUND_PRIORITY, 0, 1f);
        }

        score -=15*level;//quita algunos puntos
        score=Math.max(score, 0);//no permite que la puntuación sea menor que cero
        displayScores();//actualiza la puntuación/nivel en la pantalla

        return true;
    }


    /**
     * Hilo que crea un nuevo spot
     */
    private Runnable addSpotRunnable=new Runnable() {
        @Override
        public void run() {
            addNewSpot();
        }
    };
    /**
     * Añade un nuevo spot a la pantalla
     */
    public void addNewSpot(){

        //se crean dos coordenadas aleatorias para el comienzo y el final de los puntos
        int x=random.nextInt(viewWidth - SPOT_DIAMETER);
        int y=random.nextInt(viewHeigh - SPOT_DIAMETER);
        int x2=random.nextInt(viewWidth - SPOT_DIAMETER);
        int y2=random.nextInt(viewHeigh - SPOT_DIAMETER);


        //crear el spot/círculo actual
        final ImageView spot= (ImageView) inflater.inflate(R.layout.untouched, null);

        spots.add(spot);
        spot.setLayoutParams(new RelativeLayout.LayoutParams(SPOT_DIAMETER, SPOT_DIAMETER));


//        DisplayMetrics metrics=new DisplayMetrics();
//        WindowManager windowManager=(WindowManager) spot.getContext().getSystemService(Context.WINDOW_SERVICE);
//
//        windowManager.getDefaultDisplay().getMetrics(metrics);
//        int width=metrics.widthPixels;
//        int height=metrics.heightPixels;


        spot.setImageResource(random.nextInt(2)==0 ? R.drawable.green_spot:R.drawable.red_spot);

        spot.setX(x);
        spot.setY(y);


        spot.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                touchedSpot(spot);//manejamos el spot pulsado

            }
        });



        mainRelativeLayout.addView(spot);//añade un círculo a la pantalla

        //añadimos las animaciones de los spot
        spot.animate().x(x2).y(y2).scaleX(SCALE_X).scaleY(SCALE_Y)
                .setDuration(animationTime)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        animators.add(animation);//se guarda para un posible cancelado
                    }

                    public void onAnimationEnd(Animator animation) {
                        animators.remove(animation);//animación acabada y borrada

                        if(!gamePaused && spots.contains(spot)){//no pulsado
                            missedSpot(spot);//pierdes una vida
                        }
                    }
                });
    }




    /**
     * Se llama cuando un spot finaliza su animación sin ser tocado
     * @param spot
     */
    private void missedSpot(ImageView spot) {
        spots.remove(spot);//borra el spot de la lista de spots
        mainRelativeLayout.removeView(spot);//borra la view del layout

        if(gameOver){//sale si el juego está en gameover
            return;
        }

        if(soundPool!=null){//suena el efecto de desparecer
            soundPool.build().play(DISAPPEAR_SOUND_ID, volume, volume, SOUND_PRIORITY, 0, 1f);
        }

        if(livesLinearLayout.getChildCount()==0){//game over
            gameOver=true;

            if(score>highScore){//si mejoramos la mejor puntuación
                SharedPreferences.Editor editor=preferences.edit();
                editor.putInt(HIGH_SCORE, score);
                editor.apply();//almacena la nueva puntuación

                highScore=score;
            }

            cancelAnimations();

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Game Over")
                    .setMessage("Score: " + score)
                    .setPositiveButton("Reset", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            displayScores();
                            dialogDisplayed=false;
                            resetGame();
                        }
                    });

            dialogDisplayed=true;
            builder.show();

        } else {
            livesLinearLayout.removeViewAt(
                    livesLinearLayout.getChildCount() - 1
            );
            addNewSpot();
        }
    }





    private void cancelAnimations() {
        for (Animator animator:animators){
            animator.cancel();
        }

        //borrar los spots que queden en la pantalla
        for (ImageView view:spots){
            mainRelativeLayout.removeView(view);
        }

        spotHandler.removeCallbacks(addSpotRunnable);
        animators.clear();
        spots.clear();
    }


    /**
     * Cuando un spot es tocado
     * @param spot
     */
    private void touchedSpot(ImageView spot) {

        mainRelativeLayout.removeView(spot);
        spots.remove(spot);

        spotTouched++;
        score+=10*level;

        if(soundPool!=null){
            soundPool.build().play(HIT_SOUND_ID, volume, volume, SOUND_PRIORITY, 0, 1f);
        }

        if(spotTouched % NEW_LEVEL==0){
            level++;
            animationTime *= 0.95; //hace el juego más rápido un 5%

            if (livesLinearLayout.getChildCount()<MAX_LIVES){
                ImageView life = (ImageView) inflater.inflate(R.layout.life, null);
                livesLinearLayout.addView(life);
            }
        }

        displayScores();

        if (gameOver!=true){
            addNewSpot();
        }

    }
}
